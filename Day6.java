import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.math.BigInteger;
import java.util.stream.Stream;

public class Day6 {
    private static final String FILE = "theinput.txt";

    public static void main(String[] args) {

        try (var br = new BufferedReader(new FileReader(FILE))) {
            byte days = 18;
            File populationStorage = File.createTempFile("population", null);
            populationStorage.deleteOnExit();
            var bwr = new BufferedWriter(new FileWriter(populationStorage, false));

            Stream.of(br.readLine().split(",")).mapToInt(Integer::parseInt).forEach(f -> {
                try {
                    bwr.append(f + "\n");
                    bwr.flush();
                } catch (IOException e) {
                    System.out.println("can't write");
                }
            });

            // Stream.of(br.readLine().split(",")).mapToInt(Integer::parseInt).forEach(population::add);

            BigInteger zeroCount = new BigInteger("0");

            try (var br2 = new BufferedReader(new FileReader(populationStorage))) {
                while (days-- > 0) {
                    bwr.flush();
                    zeroCount = br2.lines().mapToInt(Integer::parseInt).filter(f -> f == 0)
                            .mapToObj(f -> new BigInteger("1")).reduce(new BigInteger("0"), BigInteger::add);
                    System.out.println(zeroCount);
                    for (BigInteger i = new BigInteger("0"); zeroCount.compareTo(i) > 0; i = i.add(BigInteger.ONE)) {
                        bwr.append("\n" + 9);
                    }
                    bwr.flush();

                    br2.lines().mapToInt(Integer::parseInt).map(f -> f == 0 ? 7 : f).map(f -> --f).forEach(f -> {
                        try {
                            bwr.write(f + "\n");
                        } catch (IOException e) {
                        }
                    });
                }
                BigInteger total = br2.lines().map(f -> BigInteger.ONE).reduce(BigInteger.ZERO, BigInteger::add);
                System.out.println(total);
            } catch (Exception e) {
                System.out.println("cant read file");
            }

            // System.out.println(population.size());

            bwr.close();
        } catch (Exception e) {
        }
    }
}