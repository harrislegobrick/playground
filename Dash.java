import java.util.Scanner;

class Dash {
    public static void main(String[] args) {
        try (var sc = new Scanner(System.in)) {
            System.out.print("Enter two numbers, seperated by a dash: ");
            var input = sc.nextLine().split("-");
            var input1 = Integer.parseInt(input[0]);
            var input2 = Integer.parseInt(input[1]);
            var output1 = input1 == 1 ? "one" : input1 == 2 ? "two" : input1 == 3 ? "three" : "unknown number";
            var output2 = input2 == 1 ? "one" : input2 == 2 ? "two" : input2 == 3 ? "three" : "unknown number";
            System.out.printf("%s dash %s%n", output1, output2);
        }
    }
}