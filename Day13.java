import java.util.List;
import java.util.stream.Collectors;

public class Day13 {

    public static void main(String[] args) {
        try (var br = new java.io.BufferedReader(new java.io.FileReader("input.txt"))) {
            List<String> inn = br.lines().collect(Collectors.toList());
            List<Point> points = inn.stream().filter(p -> p.contains(",")).map(p -> {
                String[] split = p.split(",");
                return new Point(Integer.parseInt(split[0]), Integer.parseInt(split[1]));
            }).collect(Collectors.toList());
            List<String> instructions = inn.stream().filter(i -> i.contains("fold")).map(i -> i.split(" ")[2])
                    .collect(Collectors.toList());
            int yMax = points.stream().mapToInt(Point::y).max().getAsInt() + 1;
            int xMax = points.stream().mapToInt(Point::x).max().getAsInt() + 1;
            // System.out.println(xMax);
            char[][] board = new char[yMax][xMax];
            for (int i = 0; i < yMax; i++) {
                for (int j = 0; j < xMax; j++) {
                    final int jj = j, ii = i;
                    board[i][j] = points.stream().filter(p -> p.x() == jj && p.y() == ii).count() > 0 ? '#' : '.';
                }
            }

            // printBoard(board);
            for (int i = 0; i < yMax; i++) {
                for (int j = 0; j < xMax; j++) {
                    final int jj = j, ii = i;
                    char replacement = board[i][j];
                    switch (instructions.get(0).charAt(0)) {
                        case 'y':
                            int y = Integer.parseInt(instructions.get(0).split("=")[1]);
                            replacement = i == y ? '-' : replacement;
                            break;
                        case 'x':
                            int x = Integer.parseInt(instructions.get(0).split("=")[1]);
                            replacement = j == x ? '|' : replacement;

                    }
                    board[i][j] = replacement;
                }
            }

            printBoard(board);

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public static void printBoard(char[][] board) {
        for (int i = 0; i < board.length; i++) {
            for (int j = 0; j < board[0].length; j++) {
                System.out.print(board[i][j] + " ");
            }
            System.out.println();
        }
    }

}

class Line {
    private final Point p1, p2;

    public Line(Point p1, Point p2) {
        this.p1 = p1;
        this.p2 = p2;
    }

    public Point p2() {
        return p2;
    }

    public Point p1() {
        return p1;
    }
}

class Point {
    private final int x, y;

    public Point(int x, int y) {
        this.x = x;
        this.y = y;
    }

    public int x() {
        return x;
    }

    public int y() {
        return y;
    }
}