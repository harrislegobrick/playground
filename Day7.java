import java.util.stream.IntStream;
import java.util.stream.Stream;

public class Day7 {
    public static void main(String[] args) {
        try (var br = new java.io.BufferedReader(new java.io.FileReader("theinput.txt"))) {
            Stream<String> input = br.lines();

            int[] crabsX = Stream.of(input.findFirst().get().split(",")).mapToInt(Integer::parseInt).toArray();
            int min = IntStream.of(crabsX).min().getAsInt();
            int max = IntStream.of(crabsX).max().getAsInt();

            int[] moveCosts = new int[max - min];

            for (int location = min; location < max; location++) {
                final int locationF = location;
                moveCosts[location] = IntStream.of(crabsX).map(crabLocation -> Math.abs(locationF - crabLocation)).sum();
            }
            System.out.println(IntStream.of(moveCosts).min().getAsInt());
        } catch (Exception e) {
        }
    }
}
