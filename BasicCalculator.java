import java.util.Scanner;

public class BasicCalculator {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);

        System.out.print("Enter how many calculations to make: ");
        String initalInput = sc.nextLine();
        int numOfCalculations = Integer.parseInt(initalInput);

        while (numOfCalculations > 0) {
            int result;

            System.out.print("Enter a whole number: ");
            String input = sc.nextLine();
            int firstNum = Integer.parseInt(input);

            System.out.print("Enter another whole number: ");
            String input2 = sc.nextLine();
            int secondNum = Integer.parseInt(input2);

            System.out
                    .println("Enter what you want to do (addition, subtraction, power, multiplication, or division): ");
            String input3 = sc.nextLine();

            result = calculate(input3, firstNum, secondNum);

            System.out.println("Your answer is: " + result);
            numOfCalculations--;
        }

        sc.close();
    }

    public static int calculate(String input, int firstNum, int secondNum) {
        int result = 1;
        if (input.equals("addition")) {
            result = firstNum + secondNum;
        } else if (input.equals("subtraction")) {
            result = firstNum - secondNum;
        } else if (input.equals("multiplication")) {
            result = firstNum * secondNum;
        } else if (input.equals("division")) {
            result = firstNum / secondNum;
        } else if (input.equals("power")) {
            for (int i = 0; i < secondNum; i++) {
                result = result * firstNum;
            }
        } else {
            System.out.println("Not one of the above options!!");
            System.exit(0);
        }

        return result;
    }

}
