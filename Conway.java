import java.util.*;

class Conway {
    private static int boardSize = 10;
    private static char alive = '1';
    private static char dead = '0';

    public static void main(String[] args) {
        try (Scanner sc = new Scanner(System.in)) {
            int testCases = Integer.parseInt(sc.nextLine());
            for (int currentCase = 0; currentCase < testCases; currentCase++) {

                int numGens = Integer.parseInt(sc.nextLine());

                char[][] currentBoard = new char[boardSize][boardSize];
                // import board
                for (int y = 0; y < boardSize; y++) {
                    String line = sc.nextLine();
                    for (int x = 0; x < boardSize; x++) {
                        currentBoard[x][y] = line.charAt(x);
                    }
                }
                // import board

                for (int genLoop = 0; genLoop < numGens; genLoop++) {
                    char[][] newBoard = new char[boardSize][boardSize];
                    for (int y = 0; y < boardSize; y++) {
                        for (int x = 0; x < boardSize; x++) {
                            int numAdj = 0;

                            // check for number of adjacent tiles
                            for (int y2 = y - 1; y2 < y + 2; y2++) {
                                for (int x2 = x - 1; x2 < x + 2; x2++) {
                                    if (x2 > -1 && x2 < boardSize && y2 > -1 && y2 < boardSize
                                            && !(x2 == x && y2 == y)) {
                                        if (currentBoard[x2][y2] == alive) {
                                            numAdj++;
                                        }
                                    }
                                }
                            }
                            // check for number of adjacent tiles

                            // rules
                            if ((numAdj == 0 || numAdj == 1) && currentBoard[x][y] == alive) {
                                newBoard[x][y] = dead;
                            } else if ((numAdj == 2 || numAdj == 3) && currentBoard[x][y] == alive) {
                                newBoard[x][y] = alive;
                            } else if (numAdj > 3 && currentBoard[x][y] == alive) {
                                newBoard[x][y] = dead;
                            } else if (numAdj == 3 && currentBoard[x][y] == dead) {
                                newBoard[x][y] = alive;
                            } else {
                                newBoard[x][y] = dead;
                            }
                            // rules
                        }
                    }
                    currentBoard = newBoard;
                }
                printBoard(currentBoard);
            }
        }
    }

    private static void printBoard(char[][] board) {
        System.out.println();
        for (int y = 0; y < boardSize; y++) {
            for (int x = 0; x < boardSize; x++) {
                System.out.print(board[x][y]);
            }
            System.out.println();
        }
    }
}
