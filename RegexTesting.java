import java.util.regex.MatchResult;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class RegexTesting {
    public static void main(String[] args) {
        // tests for a phone number
        // Pattern p = Pattern.compile("\\(?912\\)?-?[0-9]{3}-?[0-9]{4}");
        // String input = "(912)0001234 9210001234 912-409-1111 912-4105150
        // (912)-401-1414 9124041234";

        // ip address checker
        String zeroTo255 = "(([01][0-9]{2})|(2[0-4][0-9])|(25[0-5]))";
        Pattern p = Pattern.compile(String.format("(%s\\.){3}%<s", zeroTo255));
        String input = "000.000.000.000 125.212.145.001 255.255.001.000";
        Matcher m = p.matcher(input);

        m.results().map(MatchResult::group).forEach(System.out::println);
    }
}
