import java.io.BufferedReader;
import java.io.FileReader;

class HomeKeys {
    public static void main(String[] args) throws Exception {
        var reader = new BufferedReader(new FileReader("input.txt"));
        reader.lines().filter(s -> s.chars().noneMatch(Character::isDigit)).forEach(System.out::println);

        reader.close();
    }
}