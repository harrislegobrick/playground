import java.io.*;
import java.util.*;

public class LineRemover2 {
    static File file = new File("inputter.txt");
    static ArrayList<String> words = new ArrayList<String>();

    public static void main(String[] args) throws Exception {
        System.out.println("Compile success!!");
        try (var reader = new BufferedReader(new FileReader(file)); var sc = new Scanner(System.in)) {
            reader.lines().forEachOrdered(words::add); // adds the words in the file to the list
            System.out.println(words); // prints the words in the list from the file
            removeText(sc.nextLine()); // removes the user inputed text
            readFile(file.getName()); // prints the contents of the file
        }
    }

    static void removeText(String text) throws IOException {
        try (FileWriter fw = new FileWriter(file, true)) {
            new FileOutputStream(file).close();
            words.stream().filter(s -> !s.equals(text)).forEach(s -> {
                try {
                    fw.write(s + "\n");
                } catch (IOException e) {
                    System.out.println("Can't write to file!!");
                }
            });
        }
    }

    public static void readFile(String file) throws IOException {
        try (var br = new BufferedReader(new FileReader(file))) {
            br.lines().forEachOrdered(System.out::println);
        }
    }
}