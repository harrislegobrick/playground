import java.io.*;

public class LineRemover {
    public static void main(String[] args) throws IOException {
        removeText3("inputter.txt", "pretend this is funny");

    }

    static void removeText3(String file, String text) throws IOException {
        File temp = new File("tempFile.txt");
        try (BufferedReader br = new BufferedReader(new FileReader(file));
                FileWriter write = new FileWriter(temp, true)) {
            br.lines().filter(s -> !s.equals(text)).forEach(t -> {
                try {
                    write.write(t + "\n");
                } catch (IOException e) {
                }
            });
            temp.renameTo(new File(file));
        }
    }

    static void removeText4(String file, String text) throws IOException {
        File temp = File.createTempFile("the", null);
        try (BufferedReader br = new BufferedReader(new FileReader(file));
                FileWriter write = new FileWriter(temp, true)) {
            br.lines().filter(s -> !s.equals(text)).forEach(t -> {
                try {
                    write.write(t + "\n");
                } catch (IOException e) {
                }
            });
            temp.renameTo(new File(file));
        }
    }
}