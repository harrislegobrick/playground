import java.util.Scanner;
import java.util.stream.Stream;
import java.io.File;
import java.io.FileNotFoundException;
import java.util.InputMismatchException;

public class Exception2 {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        int total = 0;
        int num = 0;

        File myFile = null;
        Scanner inputFile = null;
        myFile = new File("inFile.txt");
        try {
            inputFile = new Scanner(myFile);
            while (inputFile.hasNext()) {
                try {
                    num = inputFile.nextInt();
                    total += num;
                } catch (InputMismatchException ex) {
                    System.out.println(inputFile.next() + " is not valid");
                    inputFile.next();
                }

            }
        } catch (FileNotFoundException e) {
            System.err.println("File not found!!");
        } finally {
            scan.close();
        }

        System.out.println("The total value is " + total);

        // custom
        try (var br = new java.io.BufferedReader(new java.io.FileReader("inFile.txt"))) {
            System.out.printf("The total value is %d.%n", br.lines().mapToInt(i -> Stream.of(i.split(" ")).mapToInt(s -> {
                try {
                    return Integer.parseInt(s);
                } catch (Exception e) {
                    System.err.printf("%s is not valid!!%n", s);
                    return 0;
                }
            }).sum()).sum());
        } catch (FileNotFoundException e) {
            System.err.println("File not found!!");
        } catch (java.io.IOException e) {
            System.err.println("Something wrong with closing the reader!! (idk)");
        }
    }
}